package advancedjavasolutions;

import java.util.HashMap;
import java.util.Map;

public class SearchKeyMap {

    /** * Search the given for the key. If the key is found in the map, return the value.
     * Otherwise return the String "NOT_FOUND". */
    public static String findValue(Map<String, String> map,  String searchKey) {
       /* String result=map.containsKey(searchKey) ? (map.get(searchKey)) :
                ("NOT_FOUND");
        return result;*/

        return map.entrySet().stream()
                .filter(e-> e.getKey().equals(searchKey))
                .findFirst()
                .map(Map.Entry::getValue) // return the key of the matching entry if found
                .orElse("NOT_FOUND");
    }



    public static void main(String [] args){
        Map<String, String> map = new HashMap<>();
        map.put("1", "One");
        map.put("2", "Two");
        map.put("3", "Three");
        String searchKey="3";
        System.out.println(SearchKeyMap.findValue(map,searchKey));


    }
}
