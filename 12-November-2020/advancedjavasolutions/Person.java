package advancedjavasolutions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Person {

     String firstName;
     String lastName;
     int age;
    private static final Random r = new Random(Integer.MAX_VALUE);




    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }

    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;



    }

    public static boolean isOldPerson(Person person)  {
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(person.age<40){
            return false;
        }else{
            return true;
        }

    }

    public static Person create() {
        //Get the employee id in more predictable manner
        //e.g. Max id present in databse + 1

        int min = 10;
        int max = 50;
        Person obj = new Person("Person"+r.nextInt(), "lastname"+r.nextInt(),  (int)(Math.random() * (max - min + 1) + min));

        return obj;
    }



    public static void main (String [] args){
       //First way of creating Person Object
       /* int min = 10;
        int max = 50;


       List<Person> list=new ArrayList<>();
        for(int i=0;i<10000;i++){
            Person person=new Person("p"+i, "pp"+i, (int)(Math.random() * (max - min + 1) + min));
            list.add(person);

        }*/

        //Second way of creating a person Object
       List<Person> list= Stream.generate(Person::create)
               .limit(10000)
               .collect(Collectors.toList());


       // list.stream().forEach(System.out::println);

        long timeBefore = System.currentTimeMillis();
        System.out.println("Before filtering");
        System.out.println("Before filtering "+ timeBefore);
        //1st way
       /*List<Person> list1= list.parallelStream().filter(person -> !(isOldPerson(person)))
                .collect(Collectors.toList());*/
        //2nd way
        /*list.parallelStream().filter(person -> !(isOldPerson(person)))
                .forEach(System.out::println);*/
        //3rd way
        /*list.parallelStream().filter(person -> !(isOldPerson(person)))
                .map(person -> new Person(person.firstName, person.lastName, person.age))
                .forEach(System.out::println);*/
        //4th way(without using isOldPerson
        list.parallelStream().filter(person -> (person.age>40)).collect(toList())
                .forEach(System.out::println) ;




        //list1.stream().forEach(System.out::println);
        //System.out.println("Size:"+list1.size());
        long timeAfter = System.currentTimeMillis();
        System.out.println("After filtering "+ (timeAfter-timeBefore));




    }
}
