package advancedjavasolutions;

import java.util.Arrays;
import java.util.List;

//Given a List<Integer> as the input, write a function that returns the average of all elements
public class AverageOfAll {



    public static double getFlattenList(List<Integer> list){
        // mapToInt method does the following:
        /*Returns an IntStream consisting of the results of applying the given function to the elements of this stream.
        This is an intermediate operation.*/
        Double average = list.stream().mapToInt(val -> val).average().orElse(0.0);
        return average;
    }

    public static void main(String [] args){

        List<Integer> list = Arrays.asList(2,4,55,7,77,45);
        System.out.println(AverageOfAll.getFlattenList(list));

    }


    //average does the following:
    //Returns an OptionalDouble describing the arithmetic mean of elements of this stream,
    // or an empty optional if this stream is empty. This is a special case of a reduction.
    //This is a terminal operation.
}
