package advancedjavasolutions;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

//GroupingBy collector is used
// for grouping objects by some property and storing results in a Map instance.
//Notice that the second argument of the groupingBy method is a Collector
// and you are free to use any Collector of your choice.

public class ListToMap {


    public static  void getMap(List<String> list){
    Map<Integer, List<String>> result = list.stream()
            .collect(Collectors.groupingBy(
                    String::length,        // use length of string as key
                    TreeMap::new,          // create a TreeMap
                    Collectors.toList())); // the values is a list of strings

       /* Map<Integer, Set<String>> result = list.stream()
                .collect(groupingBy(String::length, toSet()));*/

        System.out.println(result);

}

    public static void main(String [] args){

        List<String> list = Arrays.asList("Kavya","Nadella","Hi","Hello");
       ListToMap.getMap(list);

    }

}
