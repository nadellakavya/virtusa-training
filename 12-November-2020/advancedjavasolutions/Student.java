package advancedjavasolutions;

import java.util.*;
import java.util.stream.Collectors;

public class Student {
    private String id;
    private Map<String, Integer> report;


    public Student(String id, Map<String, Integer> report) {
        this.id = id;
        this.report = report;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Integer> getReport() {
        return report;
    }

    public void setReport(Map<String, Integer> report) {
        this.report = report;
    }

    @Override
    public String toString() {
        return "Student{" + "id='" + id + '}';
    }

    public static Map<Subject, List<Student>> getBestScores(List<Student> students) {

        Map<Subject, List<Student>>  result=new HashMap<>();
        for(int i=0;i<Subject.values().length;i++){
            final int j=i;
            Integer topScore=students.stream()
                    .mapToInt(student->student.report.get(Subject.values()[j].toString()))
                    .max()
                    .orElseThrow(NoSuchElementException::new);

            List<Student> toppers=students
                    .stream()
                    .filter(student -> student.report.get(Subject.values()[j].toString()).equals(topScore))
                            .sorted(Comparator.comparingInt((Student a)->a.report.get(Subject.values()[j].toString())).thenComparing((
                                    Student a)->a.id))
                            .collect(Collectors.toList());
                    result.put(Subject.values()[j],toppers);
        }
        return result;

        }




    public static void main(String [] args){
        HashMap<String,Integer> map1=new HashMap<String,Integer>();
        map1.put(Subject.SCIENCE.toString(), 50);
        map1.put(Subject.MATHEMATICS.toString(), 30);
        map1.put(Subject.LANGUAGE.toString(), 70);
        map1.put(Subject.HISTORY.toString() , 25);
        Student s1=new Student("1",map1);


        HashMap<String,Integer> map2=new HashMap<String,Integer>();
        map2.put(Subject.SCIENCE.toString(), 53);
        map2.put(Subject.MATHEMATICS.toString(), 90);
        map2.put(Subject.LANGUAGE.toString(), 80);
        map2.put(Subject.HISTORY.toString() , 35);
        Student s2=new Student("2",map2);

        HashMap<String,Integer> map3=new HashMap<String,Integer>();
        map3.put(Subject.SCIENCE.toString(), 40);
        map3.put(Subject.MATHEMATICS.toString(), 40);
        map3.put(Subject.LANGUAGE.toString(), 90);
        map3.put(Subject.HISTORY.toString() , 100);
        Student s3=new Student("3",map3);

        List<Student> list=new ArrayList<>();
        list.add(s1);
        list.add(s2);
        list.add(s3);

      
        System.out.println(Student.getBestScores(list));



    }

}
    enum Subject {
    SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
}

class Report{
    String subject;
    int marks;

    public Report(String subject, int marks) {
        this.subject = subject;
        this.marks = marks;
    }
}



