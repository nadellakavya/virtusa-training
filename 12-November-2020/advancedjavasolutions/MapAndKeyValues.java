package advancedjavasolutions;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapAndKeyValues {



    /** * This method prints Each key-value pair in a new line in the following format:
     ** KEY: {key} = VALUE: {value} * * Ex: * KEY: 1 = VALUE: JOEY * KEY: 2 = VALUE: CHANDLER */
    public static void printMapKeyValue(Map<Integer, String> map){

        /*for(Map.Entry<Integer,String> entry:map.entrySet()){
            String entryKey=Integer.toString(entry.getKey());
            String entryValue=entry.getValue();
            System.out.println( "KEY: "+ entryKey + " = VALUE:" +entryValue );
        }*/

        Map<Integer, String> result = map.entrySet()
                .stream()
                .collect(Collectors.toMap(m -> m.getKey(), m -> m.getValue()));

        System.out.println(result);


    }
    public static void main(String [] args){
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");

        MapAndKeyValues.printMapKeyValue(map);


    }


}
