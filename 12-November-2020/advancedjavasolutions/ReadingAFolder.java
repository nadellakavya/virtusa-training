package advancedjavasolutions;

import com.sun.deploy.net.MessageHeader;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class ReadingAFolder {
    public static void main(String [] args){
       File inputFolder=new File("**/**/****/**/inputFolderForReading");
        List<String> listToBeWritten=new ArrayList<>();
        try (Stream<Path> allPaths = Files.walk(Paths.get(String.valueOf(inputFolder)))) {
            allPaths.filter(Files::isRegularFile)
                    .forEach(path -> {
                        Stream<String> data = null;
                        try {
                            data = Files.lines(path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }//collect(Collectors.toList())
                       data.map(text->text.toUpperCase()).forEach(convertedText-> listToBeWritten.add(convertedText));
                        try {
                            Files.write(Paths.get("CombinedOutput.txt"), listToBeWritten);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
