package DesignPatterns.SRP;
/*This does not follow Single responsibility principle as it handles many functionalities
So we can segregate SendEmail and logError functionalities by defining different
interfaces to handle the functionality*/

public interface User {
    //Allows user to register
    boolean register(String userName,String password,String email);
    //Allows user to login if the user is registered
    boolean login(String userName,String password);
    //sends email once the user is registerd
    boolean sendEmail(String content);
    //logs error if there are some exceptions thrown while user tries to login or register
    void logError(String errorMessage);
}
