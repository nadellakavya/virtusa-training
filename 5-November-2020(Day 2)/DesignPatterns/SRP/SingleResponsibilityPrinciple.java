package DesignPatterns.SRP;


class Users{
    private Logger usingLogger;
    private EmailService usingEmailService;

    public Users(){}

    public Users(Logger usingLogger, EmailService usingEmailService) {
        this.usingLogger = usingLogger;
        this.usingEmailService = usingEmailService;
    }

    public void register(String userName, String password, String email){
        System.out.println("User "+ userName +" registered");
    }

    public void login(String userName,String password){
        System.out.println("User "+ userName +" logged in");
    }

    public Logger getUsingLogger() {
        usingLogger.logError("User using Logger to use logging functionality");
        return usingLogger;
    }

    public void setUsingLogger(Logger usingLogger) {
        this.usingLogger = usingLogger;
    }

    public EmailService getUsingEmailService() {
        usingEmailService.sendEmail("User using email service to send email");
        return usingEmailService;
    }

    public void setUsingEmailService(EmailService usingEmailService) {
        this.usingEmailService = usingEmailService;
    }
}

class Logger{
    public void logError(String errorMessage){
        System.out.println(errorMessage);
    }
}

class EmailService{
    public void sendEmail(String content){
        System.out.println(content);
    }

}

public class SingleResponsibilityPrinciple {
    public static void main(String [] args){
        Users u=new Users();
        u.register("Kavya","Kavya","nkavya10@gmail.com");
        u.login("Kavya","Kavya");
        Logger logger=new Logger();
        logger.logError("User cannot be logged in or registered");
        EmailService emailService=new EmailService();
        emailService.sendEmail("Email confirmation is sent to user");
        Users users=new Users(new Logger(),new EmailService());
        users.getUsingEmailService();
        users.getUsingLogger();

    }
}
