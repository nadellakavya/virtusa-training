package DesignPatterns.DIP;
/*
->HigherLevel modules should depend on lower level modules based on abstraction
->Abstractions should not depend on details but details should depend on abstractions
 */

class Computer{
    private HardDrive hardDrive;

    public Computer(HardDrive hardDrive){
        this.hardDrive=hardDrive;
    }

    public void getHardDriveDetails(){
        System.out.println("My HardDrive belongs to ----------"+hardDrive.getName()
        +" and my Ram is of : "+hardDrive.getRam()+ " GB");
    }


}

interface HardDrive{
    int getRam();
    String getName();
}

class Samsung implements HardDrive{

    @Override
    public int getRam() {
        return 4;
    }

    @Override
    public String getName() {
        return "Samsung";
    }
}

class Hitachi implements HardDrive{


    @Override
    public int getRam() {
        return 8;
    }

    @Override
    public String getName() {
        return "Hitachi";
    }
}
public class DependencyInversionPrinciple {
    public static void main(String [] args){

        Computer computer=new Computer(new Samsung());
        computer.getHardDriveDetails();
    }
}
