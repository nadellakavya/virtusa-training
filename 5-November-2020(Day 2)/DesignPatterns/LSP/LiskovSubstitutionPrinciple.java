package DesignPatterns.LSP;
/*Implementation Guide Lines:
    1.No new Exceptions can be thrown by subtype
    2.Client should not know which specific subtype they are calling
    3.New Derived classes just extend without replacing the functionality of old classes
 */

/*When we look at LiskovContractEmployee class, we are inheriting from LiskovEmployees
not from LiskovEmployee. So we need not implement calculateBonus() class
 */
interface Bonus{
    //Move calculateBonus method here
    double calculateBonus(double salary) ;
}

interface LiskovEmployees{
    double getMinimumSalary();
}

abstract class LiskovEmployee implements Bonus,LiskovEmployees{
    private int id;
    private String name;
    public LiskovEmployee() {
    }
    public LiskovEmployee(int id, String name) {
        this.id = id;
        this.name = name;
    }
    public abstract double calculateBonus(double salary) ;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class LiskovPermanentEmployee extends LiskovEmployee {
    public LiskovPermanentEmployee() {
    }

    public LiskovPermanentEmployee(int id, String name) {
        super(id,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.2;
    }

    public void getEmployeeType(){
        System.out.println("I am a permanent Employee");
    }

    @Override
    public double getMinimumSalary() {
        return 50000;
    }
}
class LiskovContractEmployee implements LiskovEmployees {
    int id;
    String name;
    public LiskovContractEmployee() {
    }

    public LiskovContractEmployee(int i, String name) {
        this.id=i;
        this.name=name;
    }



    public void getEmployeeType(){
        System.out.println("I am a Contractor");
    }

    @Override
    public double getMinimumSalary() {
        return 40000;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class LiskovIntern extends LiskovEmployee {

    public LiskovIntern() {
    }

    public LiskovIntern(int i, String name) {
        super(i,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.1;
    }

    public void getEmployeeType(){
        System.out.println("I am an intern");
    }

    @Override
    public double getMinimumSalary() {
        return 60000;
    }


}




public class LiskovSubstitutionPrinciple {
    public static void main(String [] args){

        LiskovEmployee permanent=new LiskovPermanentEmployee(1,"Alex");
        System.out.println(permanent.toString()+" and my bonus is: " + permanent.calculateBonus(50000)
        +" and my salary is "+permanent.getMinimumSalary());
        LiskovEmployee intern=new LiskovIntern (2,"Mike");
        System.out.println(intern.toString()+" and my bonus is: " + intern.calculateBonus(50000)
                +" and my salary is "+intern.getMinimumSalary());
        LiskovEmployees contractor=new LiskovContractEmployee(3,"Tina");
        System.out.println(contractor.toString()+" and my salary is: " + contractor.getMinimumSalary()+
                " and I don't have any bonus");

    }
}
