package DesignPatterns.LSP;
/*creating a class called ContractEmployee who is not eligible for any bonus
So creating an object of this class and calculating bonus would throw an exception
which violates LSP that states that no new exception can be thrown by the subtype
 */

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

abstract class LiskovPartialEmployees {
    private int id;
    private String name;

    public LiskovPartialEmployees() {

    }

    public LiskovPartialEmployees(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public abstract double calculateBonus(double salary) ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class LiskovPartialPermanentEmployees extends LiskovPartialEmployees {
    public LiskovPartialPermanentEmployees() {
    }

    public LiskovPartialPermanentEmployees(int id, String name) {
        super(id,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.2;
    }

    public void getEmployeeType(){
        System.out.println("I am a permanent Employee");
    }
}

class LiskovPartialInterns extends LiskovPartialEmployees {

    public LiskovPartialInterns() {
    }

    public LiskovPartialInterns(int i, String name) {
        super(i,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.1;
    }

    public void getEmployeeType(){
        System.out.println("I am an intern");
    }
}

class LiskovPartialContractors extends LiskovPartialEmployees {

    public LiskovPartialContractors() {
    }

    public LiskovPartialContractors(int i, String name) {
        super(i,name);
    }

    @Override
    public double calculateBonus(double salary) {
        throw new NotImplementedException();
    }

    public void getEmployeeType(){
        System.out.println("I am a contractor");
    }
}
public class PartialLiskov {
    public static void main(String [] args){
        LiskovPartialEmployees permanent=new LiskovPartialPermanentEmployees(1,"Alex");
        System.out.println(permanent.toString()+" and my bonus is: " + permanent.calculateBonus(50000));
        LiskovPartialEmployees intern=new LiskovPartialInterns(2,"Mike");
        System.out.println(intern.toString()+" and my bonus is: " + intern.calculateBonus(50000));
        LiskovPartialEmployees contractor=new LiskovPartialContractors(3,"Tina");
        System.out.println(contractor.toString()+" and my bonus is: " + contractor.calculateBonus(50000));


    }
}
