package DesignPatterns.ISP;

interface BasicPrinter{
    boolean printText(String text);
    boolean scanText(String text);
    boolean photoCopyText(String text);
}

interface AdvancedPrinter extends BasicPrinter {
    boolean faxText(String text);
}

interface MoreAdvancedPrinter extends AdvancedPrinter{
    boolean printDuplexText(String text);
}



public class InterfaceSegregation {
    public static void main(String [] args){
        AdvancedPrinter advancedPrinter=new AdvancedPrinter() {
            @Override
            public boolean faxText(String text) {
                System.out.println("fax text using advanced printer ----"+text);
                return true;
            }

            @Override
            public boolean printText(String text) {
                System.out.println("print text using advanced printer ----"+text);
                return true;
            }

            @Override
            public boolean scanText(String text) {
                System.out.println("scan text using advanced printer ----"+text);
                return true;
            }

            @Override
            public boolean photoCopyText(String text) {
                System.out.println("photoCopy text using advanced printer ----"+text);
                return true;
            }
        };
        advancedPrinter.faxText("Hi");
        advancedPrinter.printText("This is Kavya");
        advancedPrinter.scanText("I am a software engineer");
        advancedPrinter.photoCopyText("I live in Ohio");

    }
}
