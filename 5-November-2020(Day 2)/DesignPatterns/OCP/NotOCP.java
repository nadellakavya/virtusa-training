package DesignPatterns.OCP;
//This example does not implement Open/Closed principle
/*We add an extra functionality in the same class that calculates bonus based on employee type,
and doing this in the same class breaks open closed principle
*/
class Employee{
    private int id;
    private String name;
    //For calculating bonus, we need to give employee type
    private String employeeType;

    public Employee(){

    }
    public Employee(int id,String name,String employeeType){
        this.id=id;
        this.name=name;
        this.employeeType=employeeType;
    }

    public double calculateBonus( double salary){
        if(employeeType.equals("permanent")){
          return salary * 0.2;
        }
        else{
            return salary*0.1;
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employeeType='" + employeeType + '\'' +
                '}';
    }
}

public class NotOCP {
    public static void main(String [] args){
        Employee permanent=new Employee(1,"William","permanent");
        System.out.println(permanent.toString()+" and the bonus is :  " +permanent.calculateBonus(5000));
        Employee intern=new Employee(2,"John","intern");
        System.out.println(intern.toString()+" and the bonus is :  " +intern.calculateBonus(5000));
    }

}
