package DesignPatterns.OCP;
/*Creating separate classes for permanent and intern employees and extending the employee class
to implement Open close principle
*/
abstract class Employees {
    private int id;
    private String name;

    public Employees() {

    }

    public Employees(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public abstract double calculateBonus(double salary) ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}

class PermanentEmployees extends Employees{
    public PermanentEmployees() {
    }

    public PermanentEmployees(int id, String name) {
       super(id,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.2;
    }

    public void getEmployeeType(){
        System.out.println("I am a permanent Employee");
    }
}

class Interns extends Employees{

    public Interns() {
    }

    public Interns(int i, String name) {
        super(i,name);
    }

    @Override
    public double calculateBonus(double salary) {
        return salary*0.1;
    }

    public void getEmployeeType(){
        System.out.println("I am an intern");
    }
}


public class OpenClosePrinciple {
    public static void main(String [] args){
        Employees permanent=new PermanentEmployees(1,"Alex");
        System.out.println(permanent.toString()+" and my bonus is: " + permanent.calculateBonus(50000));
        Employees intern=new Interns(2,"Mike");
        System.out.println(intern.toString()+" and my bonus is: " + intern.calculateBonus(50000));


    }
}
