package FactoryPattern;

import FactoryPattern.AnimalCharacter.Animal;
import FactoryPattern.AnimalCharacter.Birds;
import FactoryPattern.AnimalCharacter.Horses;
import FactoryPattern.HumanCharacter.Soldiers;
import FactoryPattern.HumanCharacter.Villagers;

public class NormalCharacterFactory {

    private Game a;

    public Game createCharacter(String characterType, String objectType,Boolean panicMode)
    {
        if (characterType == null || characterType.isEmpty())
            return null;
        if (characterType.equals(EnumDeclarations.CharacterType.HUMAN)) {
            if (objectType.equals(EnumDeclarations.HumanCategory.SOLDIERS)) {
                a=new Soldiers();
                //return a;
            } else {
                a=new Villagers();
               // return a;
            }
            return a;
        }
        else if (characterType.equals(EnumDeclarations.CharacterType.ANIMAL)) {
            if(objectType.equals(EnumDeclarations.AnimalCategory.BIRDS)){
                a=new Birds();
            }else{
                a= new Horses();
            }
           return a;




        }
        return a;
    }

    public Game getA() {
        return a;
    }

    public void setA(Game a) {
        this.a = a;
    }
}

