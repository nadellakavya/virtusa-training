package FactoryPattern.HumanCharacter;

import FactoryPattern.EnumDeclarations;
import FactoryPattern.Game;

public abstract class Human implements Game {
    private int height;
    private EnumDeclarations.Gender gender;


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public EnumDeclarations.Gender getGender() {
        return gender;
    }

    public void setGender(EnumDeclarations.Gender gender) {
        this.gender = gender;
    }
}
