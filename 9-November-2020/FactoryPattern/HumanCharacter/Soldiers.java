package FactoryPattern.HumanCharacter;

import FactoryPattern.EnumDeclarations;

public class Soldiers extends Human{

    private EnumDeclarations.SoldierType soldierType;

    @Override
    public EnumDeclarations.Speed setSpeed(EnumDeclarations.Speed speed) {
        return EnumDeclarations.Speed.SLOW;
    }



    public EnumDeclarations.SoldierType getSoldierType() {
        return soldierType;
    }

    public void setSoldierType(EnumDeclarations.SoldierType soldierType) {
        this.soldierType = soldierType;
    }
}
