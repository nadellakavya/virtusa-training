package FactoryPattern.HumanCharacter;

import FactoryPattern.EnumDeclarations;
import FactoryPattern.HumanCharacter.Human;

public class Villagers extends Human {
    @Override
    public EnumDeclarations.Speed setSpeed(EnumDeclarations.Speed speed) {
        return EnumDeclarations.Speed.SLOW;
    }
}
