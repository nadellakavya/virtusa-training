package FactoryPattern;

public class PanicFactory extends NormalCharacterFactory {
   NormalCharacterFactory normalCharacterFactory=new NormalCharacterFactory();
   public void resetSpeed(){
      normalCharacterFactory.getA().setSpeed(EnumDeclarations.Speed.FAST);
   }
}
