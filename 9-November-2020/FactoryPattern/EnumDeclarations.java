package FactoryPattern;

public class EnumDeclarations {
    public enum Gender {
        MALE,
        FEMALE
    }

    public enum Speed {
        FAST,
        SLOW

    }

    public enum SoldierType{
        SWORDSMEN,
        MARKSMEN,
        SPIES
    }

    public enum AnimalType{
        FLYING,
        NONFLYING
    }

    public enum Environment {
        STONEAGE,
        MEDIEVAL
    }

    public enum CharacterType{
        ANIMAL,
        HUMAN
    }

    public enum HumanCategory{
        VILLAGERS,
        SOLDIERS
    }

    public enum AnimalCategory{
        HORSES,
        BIRDS
    }
}
