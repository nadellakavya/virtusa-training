package FactoryPattern.AnimalCharacter;

import FactoryPattern.EnumDeclarations;

public class Horses extends Animal {

    private EnumDeclarations.AnimalType animalType;
    @Override
    public EnumDeclarations.Speed setSpeed(EnumDeclarations.Speed speed) {
        return EnumDeclarations.Speed.SLOW;
    }

    public EnumDeclarations.AnimalType getAnimalType() {
        return EnumDeclarations.AnimalType.NONFLYING;
    }

    public void setAnimalType(EnumDeclarations.AnimalType animalType) {
        this.animalType = animalType;
    }
}
