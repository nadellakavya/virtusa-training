package FactoryPattern.AnimalCharacter;

import FactoryPattern.EnumDeclarations;

public class Birds extends Animal {
    private EnumDeclarations.AnimalType animalType;
    @Override
    public EnumDeclarations.Speed setSpeed(EnumDeclarations.Speed speed) {
        return EnumDeclarations.Speed.SLOW;
    }

    public EnumDeclarations.AnimalType getAnimalType() {
        return EnumDeclarations.AnimalType.FLYING;
    }

    public void setAnimalType(EnumDeclarations.AnimalType animalType) {
        this.animalType = animalType;
    }
}
