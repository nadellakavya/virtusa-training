import FactoryPattern.NormalCharacterFactory;
import FactoryPattern.EnumDeclarations;

public class Demo {
    public static void main(String [] args){
        // I want to update the object of NormalCharacterFactory depending on the panicMode variable in the
        //PanicFactory class
        NormalCharacterFactory c=new NormalCharacterFactory();
        c.createCharacter("HUMAN","SOLDIERS",true);
    }
}
