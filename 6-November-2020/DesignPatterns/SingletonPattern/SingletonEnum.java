package DesignPatterns.SingletonPattern;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

public class SingletonEnum {

    enum Single{
        INSTANCE;
        int i;
        public void show(){
            System.out.println(i);
        }
    }
}
