package DesignPatterns.SingletonPattern;

public class SingletonEnumMain {
    public static void main(String[] args) {
        SingletonEnum.Single s1= SingletonEnum.Single.INSTANCE;
        s1.i=5;
        s1.show();
        SingletonEnum.Single s2=SingletonEnum.Single.INSTANCE;
        s2.i=7;
        System.out.println("After Changing i through s2, it reflects in s1 as they both point to same object");
        s1.show();
        s2.show();
        System.out.println(s1==s2);
    }

}
