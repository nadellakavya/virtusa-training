import java.util.Timer;
import java.util.TimerTask;
import java.awt.Toolkit;

public class ThreadScheduleAtFixedRate {
    public static void main(String args[]) {
        System.out.println("About to schedule task.");
        new AnnoyingBeepTwo();
        System.out.println("Task scheduled.");
    }
}


/**
 * Schedule a task that executes once every second.
 */

class AnnoyingBeepTwo {
    Toolkit toolkit;
    Timer timer;
//ScheduleAtFixedRate() method takes an extra parameter which
// is for repetition of the task again & again on specific time interval while
//schedule() method will execute at once
    public AnnoyingBeepTwo() {
        toolkit = Toolkit.getDefaultToolkit();
        timer = new Timer();
        timer.scheduleAtFixedRate(new RemindTask(),
                0,        //initial delay
                10*1000);  //subsequent rate
    }

    class RemindTask extends TimerTask {
        int numWarningBeeps = 3;



        public void run() {
            if (numWarningBeeps-- > 0) {
                long time = System.currentTimeMillis();
                if (time - scheduledExecutionTime() > 5) {
                    return;
                }

                //If it's not too late, beep.
                toolkit.beep();
                System.out.println("Beep!");
            } else {
                toolkit.beep();
                System.out.println("Time's up!");
                //timer.cancel(); //Not necessary because we call System.exit
                System.exit(0);   //Stops the AWT thread (and everything else)
            }
        }
    }
}
