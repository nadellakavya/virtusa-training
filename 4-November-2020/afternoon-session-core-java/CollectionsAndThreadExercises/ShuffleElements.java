

import java.util.Collections;
import java.util.LinkedList;

//Write a Java program to shuffle the elements in a linked list
public class ShuffleElements {
    public static void main(String[] args) {
        // write your code here
        LinkedList<Integer> arr = new LinkedList<>();
        arr.add(1);
        arr.add(5);
        arr.add(7);
        arr.add(30);
        arr.add(50);
        System.out.println("Before Shuffling: "+arr);
        Collections.shuffle(arr);
        System.out.println("After Shuffling: "+arr);


    }

}
