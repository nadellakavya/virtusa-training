import java.util.Collections;
import java.util.Comparator;
import java.util.NavigableMap;
import java.util.TreeMap;

//Write a Java program to sort keys in Tree Map by using comparator.

class Sorting implements Comparator<Integer> {
    @Override
    public int compare(Integer a,Integer b){
        return a.compareTo(b);
    }

}
public class SortKeysInTreeMap {
    public static void main(String [] args){
        TreeMap<Integer,String> map=new TreeMap<Integer,String>(new Sorting()) ;

        map.put(4,"Kavya");
        map.put(2,"Bhargav");
        map.put(3,"Aparna");
        System.out.println("The map is: ");
        map.forEach((key,value)-> System.out.println(key + ":"+ value));





    }

}


