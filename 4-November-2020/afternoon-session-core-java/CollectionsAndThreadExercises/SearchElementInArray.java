

import java.util.ArrayList;

//Write a Java program to search an element in a array list.
public class SearchElementInArray {

    public boolean searchElementByValue(ArrayList<Integer> arr, int x){
        for(int i=0;i<arr.size();i++){
            if(arr.get(i) == x){
                return true;
            }
        }
        return false;
    }

    public int searchElementByIndex(ArrayList<Integer> arr, int index){
        if(index<=arr.size()) return arr.get(index);
        else return -1;
    }
    public int getIndex(ArrayList<Integer> arr, int element){
        for(int i=0;i<arr.size();i++){
            if(arr.get(i) == element){
                return i;
            }
        }
        return -1;
    }





    public static void main(String[] args) {
        // write your code here
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(1);
        arr.add(5);
        arr.add(7);

        SearchElementInArray a = new SearchElementInArray();
         System.out.println(a.searchElementByValue(arr,7));
        System.out.println(a.searchElementByIndex(arr,5));
        System.out.println(a.getIndex(arr,5));
           }
}
