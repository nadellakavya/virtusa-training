

import java.util.Iterator;
import java.util.LinkedList;


//Write a Java program to iterate a linked list in reverse order.
public class IterateLinkedList {

    public void reverseIteration(LinkedList<Integer> arr){
        Iterator it = arr.descendingIterator();
        while(it.hasNext()){
            System.out.print(it.next()+" ");
        }

    }

    public static void main(String[] args) {
        // write your code here
        LinkedList<Integer> arr = new LinkedList<>();
        arr.add(1);
        arr.add(5);
        arr.add(7);
        IterateLinkedList iterateLinkedList=new IterateLinkedList();
        iterateLinkedList.reverseIteration(arr);

    }
}