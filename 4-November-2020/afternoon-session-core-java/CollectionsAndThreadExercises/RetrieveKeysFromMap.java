import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

//Write a Java program to get a portion of a map whose keys are greater than to a given key.
public class RetrieveKeysFromMap {
public static void main(String [] args){
    NavigableMap<Integer,String> map=new TreeMap<>();
    map.put(1,"Kavya");
    map.put(2,"Bhargav");
    map.put(3,"Aparna");
    System.out.println("The map is: ");
    map.forEach((key,value)-> System.out.println(key + ":"+ value));
    System.out.println("");
    System.out.println("Output is: ");
    System.out.println(map.tailMap(2,true));
}

}
