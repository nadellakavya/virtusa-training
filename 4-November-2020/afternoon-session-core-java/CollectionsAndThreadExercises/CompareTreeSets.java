
import java.util.TreeSet;

//Write a Java program to compare two tree sets.
public class CompareTreeSets {
    public static void main(String[] args) {
        TreeSet<Character> arr = new TreeSet<>();
        arr.add('K');
        arr.add('A');
        arr.add('V');

        TreeSet<Character> arr1= new TreeSet<>();
        arr1.add('Y');
        arr1.add('A');

        TreeSet<Character> result = new TreeSet<>();
        for(Character c:arr){
            System.out.println(arr1.contains(c) ? "YES": "NO");
        }

    }
}
