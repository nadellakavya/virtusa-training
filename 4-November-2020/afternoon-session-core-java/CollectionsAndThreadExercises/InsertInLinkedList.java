import java.util.LinkedList;

//Write a Java program to insert some elements at the specified position into a linked list
public class InsertInLinkedList {

    public static void main(String[] args) {
        // write your code here
        LinkedList<Integer> arr = new LinkedList<>();
        arr.add(1);
        arr.add(5);
        arr.add(7);
        System.out.println("Before Insertion: "+arr);
        LinkedList<Integer> arr1 = new LinkedList<>();
        arr1.add(30);
        arr1.add(50);

        arr.addAll(0,arr1);

        System.out.println("After Insertion: "+arr);


    }

}
