import java.util.NavigableMap;
import java.util.TreeMap;
//Write a Java program to get a reverse order view of the keys contained in a given map.
public class ReverseViewOfKeys {

    public static void main(String [] args){
        NavigableMap<Integer,String> map=new TreeMap<>();
        map.put(1,"Kavya");
        map.put(2,"Bhargav");
        map.put(3,"Aparna");
        System.out.println("The map is: ");
        map.forEach((key,value)-> System.out.println(key + ":"+ value));
        System.out.println("");
        System.out.println("Reverse view of Keys is: ");
        System.out.println(map.descendingKeySet());
        System.out.println("Reverse view of map is: ");
        System.out.println(map.descendingMap());


    }
}
