

import java.util.ArrayList;
import java.util.Collections;
//Write a Java program to copy one array list into another
public class CopyArrayList {

    public void copyContentsToNewList(ArrayList<Integer> arr){
        //need to typecast as return type of clone() method is Object
        ArrayList<Integer> result=(ArrayList) arr.clone();
        for(int i=0;i<result.size();i++){
            System.out.print(result.get(i)+" ");
        }

    }
    public static void main(String[] args) {
    ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        arr.add(5);
        arr.add(7);
        arr.add(0);

        CopyArrayList copyArrayList=new CopyArrayList();
        copyArrayList.copyContentsToNewList(arr);



        ArrayList<Integer> arr2 = new ArrayList<Integer>();
        arr2.add(2);
        arr2.add(8);
        arr2.add(6);
        arr2.add(9);

        Collections.copy(arr2,arr);
        System.out.println();
        System.out.println(arr2);




    }





}
