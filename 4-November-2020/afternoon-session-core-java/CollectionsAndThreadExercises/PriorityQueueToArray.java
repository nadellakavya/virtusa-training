import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
//Write a Java program to convert a priority queue to
// an array containing all of the elements of the queue.
public class PriorityQueueToArray {
    public static void main(String [] args){
        PriorityQueue<String> arr = new PriorityQueue<String>();
        arr.add("Kavya");
        arr.add("Bhargav");
        arr.add("Aparna");
        List<String> arr1 = new ArrayList<>(arr);
        System.out.println("The new arraylist is: "+ arr1);



    }
}
