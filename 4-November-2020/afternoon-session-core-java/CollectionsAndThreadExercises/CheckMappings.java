//Write a Java program to check whether a map contains key-value mappings (empty) or not.

import java.util.HashMap;
import java.util.Map;

public class CheckMappings {
    public static void main(String [] args){
        Map<Integer,String> map=new HashMap<>();
        map.put(1,"Kavya");
        map.put(2,"Bhargav");
        map.put(3,"Aparna");
        System.out.println("The map is: ");
        map.forEach((key,value)-> System.out.println(key + ":"+ value));
        if(map.isEmpty()){
            System.out.println("The map is Empty");
        }else{
            System.out.println("The map contains elements");
        }



    }
}
