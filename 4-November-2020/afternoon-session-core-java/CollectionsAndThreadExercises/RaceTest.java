//A time interval during which a time-sharing system is processing one
//particular computer program. Also known as time quantum

//A short interval of time allotted to each user or program in a multitasking or timesharing system.
//Time slices are typically in milliseconds.

// Preemptive multitasking involves the use of an interrupt mechanism which suspends
// the currently executing process and invokes a scheduler to determine which process should execute next.
// Therefore, all processes will get some amount of CPU time at any given time.

//Today, nearly all operating systems support preemptive multitasking,
// including the current versions of Windows, macOS, Linux
public class RaceTest {
    private final static int NUMRUNNERS = 2;

    public static void main(String[] args) {
        SelfishRunner[] runners = new SelfishRunner[NUMRUNNERS];

        for (int i = 0; i < NUMRUNNERS; i++) {
            runners[i] = new SelfishRunner(i);
            runners[i].setPriority(2);
        }
        for (int i = 0; i < NUMRUNNERS; i++)
            runners[i].start();

    }
}
class SelfishRunner extends Thread {

    private int tick = 1;
    private int num;

    public SelfishRunner(int num) {
        this.num = num;
    }
   //This run() contains a tight loop that increments the integer tick and every 50,000 ticks
   // prints out the thread's identifier and its tick count.
   //When running this program on a time-sliced system, you will see messages from both threads
   // intermingled with one another. Like this:
   //This is because a time-sliced system divides the CPU into time slots and iteratively gives each
   // of the equal-and-highest priority threads a time slot in which to run
    public void run() {
        while (tick < 400000) {
            tick++;
            if ((tick % 50000) == 0)
                System.out.println("Thread #" + num + ", tick = " + tick);
            //Use yield to gives other threads of the same priority a chance to run.
                 yield();
        }
    }
}

//When running this program on a non-time-sliced system, however, you will see messages from one thread finish
// printing before the other thread ever gets a chance to print one message
//This is because a non-time-sliced system chooses one of the equal-and-highest priority threads
// to run and allows that thread to run until it relinquishes the CPU (by sleeping, yielding, finishing its job)
// or until a higher priority preempts it.

//A thread can voluntarily yield the CPU (without going to sleep or some other drastic means)
// by calling the yield() method. The yield() method gives other threads of the same priority a chance to run. If there are no equal priority threads in the "Runnable" state,
// then the yield is ignored.

//The Java runtime supports a very simple, deterministic scheduling algorithm known as fixed priority scheduling.

//The Java runtime will not preempt the currently running thread for another thread of the same priority.
// In other words, the Java runtime does not time-slice. However, the system implementation of threads underlying the Java Thread class may support time-slicing.
// Do not write code that relies on time-slicing.

//In addition, a given thread may, at any time, give up its right to execute by calling the yield() method.
// Threads can only yield the CPU to other threads of the
// same priority--attempts to yield to a lower priority thread are ignored.


// In particular, you should never write Java code that relies on time-sharing--this will practically guarantee that your program will give different results on a different computer system.

//A thread can voluntarily yield the CPU (without going to sleep or some other drastic means)
// by calling the yield() method. The yield() method gives other threads of the same priority a chance to run.
// If there are no equal priority threads in the "Runnable" state, then the yield is ignored.